./clean.sh


echo "Installing curl"
sudo apt-get install curl


echo "installing git"
sudo apt install git-all


echo "installing docker"
echo "Uninstall old versions"
#sudo apt-get remove docker docker-engine docker.io containerd runc
echo "Install using the convenience script"
sudo curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

echo "Installing docker compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version



echo "Installing sdk man"
sudo curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"


echo "installing jdk"
#sdk install java 8.0.292.j9-adpt
sdk install java 22.0.0.2.r11-grl

echo "installing sbt"
sdk install sbt




echo "installing aws cli"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

echo "installing kops"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

echo "installing kops"
# shellcheck disable=SC2046
curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/local/bin/kops


echo "installing jq"
sudo apt-get install jq


echo "installing terminator"
sudo apt-get install terminator


echo "installing copyq"
sudo add-apt-repository ppa:hluk/copyq
sudo apt update
sudo apt install copyq
sudo apt --fix-broken install
